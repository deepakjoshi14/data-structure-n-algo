import copy

def find_string_numbers( str1, str2, sum):
    char_set = set(str1+str2+sum)
    char_arr = tuple(char_set)
    total_chars = len(char_set)
    string_values = dict.fromkeys(char_set, 1)
    maximum_chances = int(str(9)*total_chars)
    result = []

    for i in range(maximum_chances):
        str_num = str(i)
        count = len(str_num)
        for j in range(total_chars):
            key = char_arr[j]
            string_values[key] = 0 if count < j+1 else str_num[j]
        num1 = map_values_to_number(string_values, str1)
        num2 = map_values_to_number(string_values, str2)
        num3 = map_values_to_number(string_values, sum)
        if num1+num2 == num3:
            if len(set(string_values.values())) == total_chars:
                string_values['num1'] = num1
                string_values['num2'] = num2
                string_values['sum'] = num3
                result.append( copy.deepcopy(string_values))
    return result

def map_values_to_number(string_values, value):
    num = ''
    for i in range(len(value)):
        num += str(string_values[value[i]])
    return int(num)

values = find_string_numbers('UBER', 'COOL', 'UNCLE')
print(values)