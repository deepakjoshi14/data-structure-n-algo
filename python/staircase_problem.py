import copy

final_stop, possible_steps = 6, [1, 3]
total_possible_routs, stair_stack = 0, [{'number': 0, 'visited': []}]

while( len(stair_stack) > 0 ):
    current_stair = stair_stack.pop()

    if current_stair['number'] == final_stop :
        print(current_stair['visited'])
        total_possible_routs += 1
        continue

    for step in possible_steps:
        stair = current_stair['number'] + step
        if stair <= final_stop:
            visited = copy.deepcopy(current_stair['visited'])
            visited.append(stair)
            stair_stack.append({'number': stair, 'visited': visited})

print('total_possible_routs', total_possible_routs)
