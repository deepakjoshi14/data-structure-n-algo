package algo.staircase;

import java.util.Stack;

public class StairCaseDemo {

    public static void main(String[] args) {
        int finalStop = 6;
        int[] possibleSteps = new int[]{1, 3};
        int totalPossibleRouts = 0;

        Stack<Stair> stairStack = new Stack<Stair>();
        stairStack.add(new Stair(0, null));

        while (!stairStack.empty()){
            Stair currentStair = stairStack.pop();
            if (currentStair.number == finalStop){
                System.out.println(currentStair.visited);
                totalPossibleRouts++;
                continue;
            }

            for (int step : possibleSteps) {
                int stair = currentStair.number + step;
                if(stair <= finalStop){
                    stairStack.add(new Stair(stair, currentStair.visited));
                }
            }
        }
        System.out.println( "totalPossibleRouts : " + totalPossibleRouts);
    }
}
