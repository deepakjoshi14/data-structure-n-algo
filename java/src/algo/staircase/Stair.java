package algo.staircase;

import java.util.ArrayList;

public class Stair {

    public int number;
    public ArrayList<Integer> visited = new ArrayList<>();

    public Stair( int number, ArrayList<Integer> visited ){
        this.number = number;
        if (visited != null){
            this.visited.addAll(visited);
        }
        this.visited.add(number);
    }
}
