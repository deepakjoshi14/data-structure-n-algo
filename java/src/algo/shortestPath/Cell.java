package algo.shortestPath;

public class Cell {

    public int rowIndex;
    public int colIndex;
    public int distance;

    public Cell( int row, int col, int dist){
        this.rowIndex = row;
        this.colIndex = col;
        this.distance = dist;
    }

}
