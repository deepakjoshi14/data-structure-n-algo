package algo.shortestPath;

import java.util.LinkedList;
import java.util.Queue;

public class ShortPath {

    public static void main(String[] args) {
        int[][] grid = {    { 1, 1, 1, 1, 0 },
                            { 0, 1, 0, 1, 0 },
                            { 0, 1, 1, 1, 0 },
                            { 0, 9, 1, 0, 0 },
                            { 0, 0, 1, 9, 0}};
        print(grid);
        Queue<Cell> queue = new LinkedList<Cell>();
        move(grid, queue, 0, 0, 0);
        while (!queue.isEmpty()){
            Cell bot = queue.poll();
            move(grid, queue, bot.rowIndex-1, bot.colIndex, bot.distance+1);
            move(grid, queue, bot.rowIndex+1, bot.colIndex, bot.distance+1);
            move(grid, queue, bot.rowIndex, bot.colIndex-1, bot.distance+1);
            move(grid, queue, bot.rowIndex, bot.colIndex+1, bot.distance+1);
        }

    }

    static void move( int[][] grid, Queue<Cell> queue, int newRow, int newCol, int newDist ){
        if(newRow >= 0 && newRow < grid.length && newCol >= 0 && newCol < grid[0].length){
            if (grid[newRow][newCol] == 1) {
                queue.add( new Cell(newRow, newCol, newDist) );
                grid[newRow][newCol] = -1;
            }else if (grid[newRow][newCol] == 9) {
                System.out.println("Distance : " + newDist);
                print(grid);
                System.exit(0);
            }
        }
    }

    static void print(int[][] grid){
        for (int i=0; i<grid.length; i++){
            for (int j=0; j<grid[i].length; j++){
                System.out.print( grid[i][j] + "    ");
            }
            System.out.println("");
        }
    }
}
