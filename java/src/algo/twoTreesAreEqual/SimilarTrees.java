package algo.twoTreesAreEqual;

import algo.pathBetweenNodes.BinarySearchTree;
import algo.pathBetweenNodes.Node;

import java.util.LinkedList;

public class SimilarTrees {

    public static void main(String[] args) {
        BinarySearchTree tree1 = new BinarySearchTree();
        tree1.add(4, tree1.root);
        tree1.add(3, tree1.root);
        tree1.add(6, tree1.root);

        BinarySearchTree tree2 = new BinarySearchTree();
        tree2.add(6, tree2.root);
        tree2.add(3, tree2.root);
        tree2.add(4, tree2.root);

        LinkedList<Integer> list1 = new LinkedList<>();
        explore(tree1.root, list1);

        LinkedList<Integer> list2 = new LinkedList<>();
        explore(tree2.root, list2);

        System.out.println(list1);
        System.out.println(list2);
        System.out.println(list1.equals(list2));
    }

    public static void explore(Node currentNode, LinkedList<Integer> listOfNodes){
        if (currentNode == null){ return; }
        explore( currentNode.leftNode, listOfNodes );
        listOfNodes.add(currentNode.value);
        explore( currentNode.rightNode, listOfNodes );
    }

}
