package algo.pathBetweenNodes;

import java.util.ArrayList;

public class BinarySearchTree {

    public Node root = null;
    ArrayList<Integer> visitedNodes = new ArrayList<>();
    int steps = 0;

    public static void main(String[] args) {
        BinarySearchTree tree = new BinarySearchTree();
        tree.add(8, tree.root);
        tree.add(5, tree.root);
        tree.add(10, tree.root);
        tree.add(9, tree.root);
        tree.add(11, tree.root);
        tree.add(4, tree.root);
        tree.add(6, tree.root);

        tree.findPath(4, tree.root);
        tree.findPath(6, tree.root);
        System.out.println("steps : " + tree.steps);
        System.out.println("visitedNodes : " + tree.visitedNodes);
    }


    public void findPath(int searchValue, Node currentNode){
        if (visitedNodes.contains(currentNode.value)){
            steps--;
        } else {
            visitedNodes.add(currentNode.value);
            steps++;
        }

        if( searchValue == currentNode.value ){
            return;
        }

        if (searchValue > currentNode.value){
            findPath(searchValue, currentNode.rightNode);
        }
        if (searchValue < currentNode.value){
            findPath(searchValue, currentNode.leftNode);
        }
    }

    public void add( int value, Node currentNode ){
        if( root == null){
            root = new Node(value, null, null);
            return;
        }

        if (value > currentNode.value){
            if (currentNode.rightNode == null){
                currentNode.rightNode = new Node(value, null, null);
            }else {
                add(value, currentNode.rightNode);
            }
            return;
        }

        if (value < currentNode.value){
            if (currentNode.leftNode == null){
                currentNode.leftNode = new Node(value, null, null);
            }else {
                add(value, currentNode.leftNode);
            }
            return;
        }

    }


}
